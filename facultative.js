/*Changing the n variable*/
    var n = 50000;

    for (var i = 0; i < n; i++) {
        array.push(Math.random() * 100);
    }

    function arrayCopy() {
        someArr = array.slice();
        return someArr;
    }
    sortedNative = arrayCopy();
    sortedNSquare = arrayCopy();
    sortedNlogN = arrayCopy();

    /*Native methods*/
    console.time('native sorting n=50000');
    sortedNative.sort(function (a, b) {
        return a - b;
    });
    console.timeEnd('native sorting n=50000');

    console.time('n-square sorting n=50000');
    /* Bubble sort */
    bubbleSort(sortedNSquare);
    console.timeEnd('n-square sorting n=50000');

    console.time('n-log-n sorting n=50000');
    /*Merge sort*/
    sortedNlogN = mSort(sortedNlogN);
    console.timeEnd('n-log-n sorting n=50000');

/*Ordered array*/
function orderedArray(){
    n = 10000, array = [], sortedNative, sortedNSquare, sortedNlogN;

    for (var i = 0; i < n; i++) {
        array.push(Math.random() * 100);
    }

    function arrayCopy() {
        someArr = array.slice();
        return someArr;
    }

    sortedNative = arrayCopy();
    sortedNSquare = arrayCopy();
    sortedNlogN = arrayCopy();

    array.sort(function (a, b) {
        return a - b;
    });

    /*Native sort*/
    console.time('native sorting array ordered');
    sortedNative.sort(function (a, b) {
        return a - b;
    });
    console.timeEnd('native sorting array ordered');

    console.time('n-square sorting array ordered');
    /* Bubble sort */
    bubbleSort(sortedNSquare);
    console.timeEnd('n-square sorting array ordered');

    console.time('n-log-n sorting array ordered');
    /*Merge sort*/
    sortedNlogN = mSort(sortedNlogN);
    console.timeEnd('n-log-n sorting array ordered');
}
orderedArray();

/*Reverse ordered*/
function reverseOrder (){
    n = 10000, array = [], sortedNative, sortedNSquare, sortedNlogN;

    for (var i = 0; i < n; i++) {
        array.push(Math.random() * 100);
    }

    function arrayCopy() {
        someArr = array.slice();
        return someArr;
    }

    sortedNative = arrayCopy();
    sortedNSquare = arrayCopy();
    sortedNlogN = arrayCopy();

    array.sort(function (a, b) {
        return a - b;
    });
    array.reverse();

    /*Native sort*/
    console.time('native sorting reverse order');
    sortedNative.sort(function (a, b) {
        return a - b;
    });
    console.timeEnd('native sorting reverse order');

    console.time('n-square sorting reverse order');
    /* Bubble sort */
    bubbleSort(sortedNSquare);
    console.timeEnd('n-square sorting reverse order');

    console.time('n-log-n sorting reverse order');
    /*Merge sort*/
    sortedNlogN = mSort(sortedNlogN);
    console.timeEnd('n-log-n sorting reverse order');
}
reverseOrder();