var n = 10000;
var array = [];

function arrayFunc (array){
    for (var i = 0; i < n; i++) {
        array.push(Math.random() * 100);
    }
}
arrayFunc(array);

function arrayCopy(){
   return array.slice();
}

var sortedNative = arrayCopy();
var sortedNSquare = arrayCopy();
var sortedNlogN = arrayCopy();

/*Native methods*/
console.time('native sorting');

function sortNative (arr){
    arr.sort(function (a, b) {
        return a - b;
    });
}
sortNative(sortedNative);

console.timeEnd('native sorting');

console.time('n-square sorting');
/* Bubble sort */
function bubbleSort(arr) {
    var arrLength = arr.length;
    for (var i = 0; i < arrLength - 1; i++) {
        for (var j = 0; j < arrLength - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                var bigger = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = bigger;
            }
        }
    }
}
bubbleSort(sortedNSquare);
console.timeEnd('n-square sorting');

console.time('n-log-n sorting');
/*Merge sort*/
function forMerge(leftPart, rightPart) {
    var resultArray = [];
    while (leftPart.length > 0 && rightPart.length > 0){
        if (leftPart[0] < rightPart[0]) {
            resultArray.push(leftPart.shift());
        }else {
            resultArray.push(rightPart.shift());
        }
    }
    return resultArray.concat(leftPart).concat(rightPart);
}

function mSort(someArr) {
    if (someArr.length == 1) {
        return someArr;
    }
    var middle = someArr.length / 2;
    var leftHalf = someArr.slice(0, middle);
    var rightHalf = someArr.slice(middle);
    return forMerge(mSort(leftHalf), mSort(rightHalf));
}
sortedNlogN = mSort(sortedNlogN);
console.timeEnd('n-log-n sorting');

//checking
function checker(array, name) {
    for (var i = 0; i < n - 2; i++) {
        if (array[i] > array[i + 1]) {
            throw 'Something went wrong with ' + name;
        }
    }
}

checker(sortedNative, 'native sorting');
checker(sortedNSquare, 'n-square sorting');
checker(sortedNlogN, 'n-log-n sorting');
